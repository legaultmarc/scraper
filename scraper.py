#!/usr/bin/env python2.7

from collections import deque
import argparse
import urllib2
import re
import urlparse
import os.path

try:
    from bs4 import BeautifulSoup
except Exception as e:
    print "Install BeautifulSoup: sudo pip install bs4"
    raise e

def main(args):
    url = args.url
    base = "/".join(url.split("/")[:-1]) + "/"
    extensions = [re.sub(r"\s+", "", ext) for ext in args.extension.split(",")]
    download(base, url, extensions, deque([]))

def download(base, url, extensions, queue = None, downloaded = []):
    stream = urllib2.urlopen(url)
    html = stream.read()
    stream.close()

    # Add links to queue
    soup = BeautifulSoup(html)
    for link in soup.find_all('a'):
        # Adjust href if it is not absolute
        href = link.get('href')
        if href:
            href = href.split("?")[0]
            # On veut des url absolus
            if not href.startswith("http://"):
                href = "{}{}".format(base, href)

            # Si finit par un slash ou .htm ou .html
            if re.match(r"(htm|html|/)$", href):
                queue.append(href)
            # Pas un .ext quelconque
            elif not re.match(r"\.[A-Za-z]{2,4}", href):
                queue.append(href)

            for ext in extensions:
                if href.endswith(ext):
                    if href not in downloaded:
                        download_file(href)
                    downloaded.append(href)
    if len(queue) > 0:
        next_url = queue.popleft()
        download(base, next_url, extensions, queue, downloaded)

def download_file(url):
    stream = None
    try:
        stream = urllib2.urlopen(url)
    except urllib2.HTTPError as e:
        print "ERROR {} for '{}'".format(e.code, url)
        return
        
    filename = os.path.split(url)[-1]
    data = stream.read()
    print "Downloading... {}".format(url)
    stream.close()

    with open(filename, "wb") as f:
        f.write(data)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                                        description = ("Downloads all files with "
                                                       "the specified --extension "
                                                       "recursively until it is stopped "
                                                       "or the script runs out of links.")
                                    )

    parser.add_argument(
                            "--extension",
                            type=str,
                            help="The extension to download (comma separated)",
                            metavar="extension",
                            dest="extension",
                            required=True,
                       )

    parser.add_argument(
                            "--url",
                            type=str,
                            help="The root url",
                            metavar="url",
                            dest="url",
                            required=True,
                       )
    args = parser.parse_args()
    try:
        main(args)
    except KeyboardInterrupt:
        print "Interrupted by user"
        exit(0)
